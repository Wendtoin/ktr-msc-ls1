import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ProfileService } from '../profileService';
import { TokenStorageService } from '../../login/_helper/localStorage';
import { SnackBarService } from 'src/app/shared/SnackBar';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss']
})
export class UserDialogComponent implements OnInit {
  public form:FormGroup;
  public passwordHide:boolean = true;
  isLoadingResults: false;
  constructor(public dialogRef: MatDialogRef<UserDialogComponent>,private snackBar: SnackBarService,
              @Inject(MAT_DIALOG_DATA) public user: any,
              public fb: FormBuilder, private profileService: ProfileService, private token : TokenStorageService) {
   this.form = fb.group({
    id: null,
    email: null,
    name:null,
    nameCompany: null,
    telephone: null,
    utilisateur:null
   })
  }

  ngOnInit() {
    if(this.user){
      this.form = this.fb.group({
        
        id: this.user.id,
        email: this.user.email,
        name:this.user.name,
        nameCompany: this.user.nameCompany,
        telephone: this.user.telephone,
        utilisateur: null,
       })
    } 
    else{
      this.form = this.fb.group({
        id: null,
        email: null,
        name:null,
        nameCompany: null,
        telephone: null,
        utilisateur: null,
       })
    } 
  }
  enregistrer()
  {
    if(this.form.valid)
    {
      let userInfo = {
        id: this.token.getUser(),
        mail: 'string',
        password: 'string'
      }

      this.form.get("utilisateur").setValue(userInfo);
      console.log(this.form.value)
      if(this.user)
      {
        this.profileService.updateprofile(this.user.id,this.form.value).subscribe((data)=>{
          close();
        },(error:HttpErrorResponse)=>{
          if(error.status<300)
          {
            this.dialogRef.close();
            this.snackBar.openSnackBar("Profile modifier avec success", "Ok", 5000);
            
          }
          else{
            this.snackBar.openSnackBar("Profile non modifier avec success", "ERROR", 5000);
          }
        })
      }
      else{
        this.profileService.addprofile(this.form.value).subscribe((data)=>{
          close();
        },(error:HttpErrorResponse)=>{
          if(error.status<300)
          {
            this.dialogRef.close();
            this.snackBar.openSnackBar("Profile enregistrer avec success", "Ok", 5000);
            
          }
          else{
            this.snackBar.openSnackBar("Profile non enregistrer avec success", "ERROR", 5000);
          }
        })
      }
    }
  }
  close(): void {
    this.dialogRef.close();
  }

}
