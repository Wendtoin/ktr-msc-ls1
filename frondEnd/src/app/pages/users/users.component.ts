import { TokenStorageService } from './../login/_helper/localStorage';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { Profile, ProfileService } from './profileService';
import { error } from '@angular/compiler/src/util';
import { SnackBarService } from 'src/app/shared/SnackBar';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  encapsulation: ViewEncapsulation.None, 
})
export class UsersComponent implements OnInit {
    public profiles: Profile[];
    public searchText: string;
    public page:any;
    public settings: Settings;
    loard = false;
    constructor(public appSettings:AppSettings, 
                private token : TokenStorageService,
                public dialog: MatDialog,
                private snackBar: SnackBarService,
                private profileService: ProfileService){
        this.settings = this.appSettings.settings; 
    }

    ngOnInit() {
        this.loard = true,
        this.profileService.getprofile(this.token.getUser()).subscribe((data)=>{
            this.profiles = data;
            console.log(this.profiles)
            this.loard = false;
        },(error: HttpErrorResponse)=>{
            console.log(error);
        })        
    }

    delete(id){
        this.profileService.deleteprofile(id).subscribe((data)=>{

        },(error:HttpErrorResponse)=>{
            if(error.status<300)
            {
                this.ngOnInit();
              this.snackBar.openSnackBar("Profile supprimer avec success", "Ok", 5000);
              
            }
            else{
              this.snackBar.openSnackBar("Profile non supprimer avec success", "ERROR", 5000);
            }
          })
    }

    public onPageChanged(event){
        this.page = event;
        if(this.settings.fixedHeader){      
            document.getElementById('main-content').scrollTop = 0;
        }
        else{
            document.getElementsByClassName('mat-drawer-content')[0].scrollTop = 0;
        }
    }

    public openUserDialog(profile){
        let dialogRef = this.dialog.open(UserDialogComponent, {
            data: profile
        });

        dialogRef.afterClosed().subscribe(user => {
            if(user == null){
                this.ngOnInit();
            }
        });
    }

}