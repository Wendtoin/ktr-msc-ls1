import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export class Profile{
    id: number;
	name : string;
	nameCompany: string;
	email: string;
	telephone: string;
}


@Injectable()
export class ProfileService {
    
    constructor(public http:HttpClient) { }
    
    getprofile(id): Observable<Profile[]> {
        return this.http.get<Profile[]>(environment.PROFILE+""+id);
    }
    addprofile(profile){	    
        return this.http.post(environment.PROFILE, profile);
    }

    deleteprofile(id: number) {
        return this.http.delete(environment.PROFILE+""+id);
    } 

    updateprofile( id, profile){	    
        return this.http.put(environment.PROFILE+""+id, profile);
    }
} 