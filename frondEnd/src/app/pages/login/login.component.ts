import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { emailValidator } from '../../theme/utils/app-validators';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { LoginService } from './authentification';
import { HttpErrorResponse } from '@angular/common/http';
import { TokenStorageService } from './_helper/localStorage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {
  public form:FormGroup;
  public settings: Settings;
  constructor(
    private token : TokenStorageService,
    public appSettings:AppSettings, 
    public fb: FormBuilder, public router:Router, 
    private loginS: LoginService){
    this.settings = this.appSettings.settings; 
    this.form = this.fb.group({
      'username': [null, Validators.compose([Validators.required, emailValidator])],
      'password': [null, Validators.compose([Validators.required])] 
    });
  }

  public onSubmit(values:Object):void {
    if (this.form.valid) {
      this.loginS.login(this.form.value).subscribe((data)=>{
        console.log(data);
        this.token.saveToken(data.token);
        this.token.saveUser(data.id);
        this.router.navigate(['/']);
      },(Error: HttpErrorResponse)=>{
  
      })
    }
  }

  ngAfterViewInit(){
    this.settings.loadingSpinner = false; 
  }
}