
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { environment } from "src/environments/environment";
import { Router } from '@angular/router';

const  httpOptions={
    headers: new HttpHeaders({'content-type': 'application/json'})
  }
  
  

  @Injectable({ providedIn: 'root' })
  export class LoginService{
    API = environment.USER;
    constructor(private http: HttpClient, private router: Router) {
     }
  
  
    login(user): Observable<any>{
      return this.http.post(this.API, {
        username:user.username,
        password:user.password},
        httpOptions);
    }

    inscription(user){
        return this.http.post(this.API+"", user);
    }
  

}