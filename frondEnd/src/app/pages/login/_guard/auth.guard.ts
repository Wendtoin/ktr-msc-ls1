
import { Injectable, OnInit } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { environment } from 'src/environments/environment';
import { TokenStorageService } from '../_helper/localStorage';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    bool = false;

    constructor(
        private router: Router,
        private token: TokenStorageService
    ) {
      
     }
 
     canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {


        if(this.token.getToken()){
            return true;
           
        }else {
            //console.log("pageFirstTime false")
                    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
                    return false;
        }
        
    }
}