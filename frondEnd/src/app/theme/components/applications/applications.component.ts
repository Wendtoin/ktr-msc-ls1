import { TokenStorageService } from './../../../pages/login/_helper/localStorage';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ApplicationsComponent implements OnInit {

  constructor(private token :TokenStorageService){ }

  ngOnInit() {
  }

  deconnection()
  {
    this.token.signOut();
    location.reload();
  }

}