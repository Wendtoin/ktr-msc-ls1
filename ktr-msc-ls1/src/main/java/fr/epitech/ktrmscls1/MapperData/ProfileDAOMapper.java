package fr.epitech.ktrmscls1.MapperData;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

import fr.epitech.ktrmscls1.entities.ProfileEntity;
import fr.epitech.ktrmscls1.models.ProfileModel;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
uses = {UtilisateurDAOMapper.class})
public interface ProfileDAOMapper {

	ProfileModel toModel(ProfileEntity profile);
	ProfileEntity toEntity(ProfileModel profile);
	List<ProfileModel> toModel(List<ProfileEntity> profile);
}
