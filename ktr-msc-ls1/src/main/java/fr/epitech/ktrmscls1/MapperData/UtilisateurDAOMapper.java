package fr.epitech.ktrmscls1.MapperData;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

import fr.epitech.ktrmscls1.entities.UtilisateurEntity;
import fr.epitech.ktrmscls1.models.UtilisateurModel;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
uses = {})
public interface UtilisateurDAOMapper {

	
	UtilisateurEntity toEntity(UtilisateurModel user);
	UtilisateurModel toModel(UtilisateurEntity user);
}
