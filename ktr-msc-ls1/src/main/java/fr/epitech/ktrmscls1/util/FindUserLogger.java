package fr.epitech.ktrmscls1.util;


import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


import fr.epitech.ktrmscls1.entities.UtilisateurEntity;
import fr.epitech.ktrmscls1.repositories.UtilisateurRepository;
import fr.epitech.ktrmscls1.security.securite.jwt.JwtUtils;

@Service
public class FindUserLogger {

		@Autowired
		private JwtUtils jwtUtils;
		
		@Autowired
		private UtilisateurRepository utilisateurDao;
		
		public UtilisateurEntity getUserWithHerRequete(HttpServletRequest requete) {
			try {
					String jwt = parseJwt(requete);
					if (jwt != null && (jwtUtils.validateJwtToken(jwt) || jwtUtils.validateJwtToken(jwt.substring(7, jwt.length())))) {
						String username = null;
							try
							{
								username = jwtUtils.getUserNameFromJwtToken(jwt.substring(7, jwt.length()));
								Optional<UtilisateurEntity> user = utilisateurDao.findByMail(username);
								if(user.isPresent())
									return user.get();
							} catch(Exception e)
							{
								username = jwtUtils.getUserNameFromJwtToken(jwt);
								Optional<UtilisateurEntity> user = utilisateurDao.findByMail(username);
								if(user.isPresent())
									return user.get();
							}
					}
				}catch (Exception e) {
					
				}
			return null;
		}
		
		private String parseJwt(HttpServletRequest request) {
			String headerAuth = request.getHeader("Authorization");

			if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
				return headerAuth.substring(7, headerAuth.length());
			}

			return null;
		}
		
}
