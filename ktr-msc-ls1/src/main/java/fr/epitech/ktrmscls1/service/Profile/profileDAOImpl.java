package fr.epitech.ktrmscls1.service.Profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.epitech.ktrmscls1.MapperData.ProfileDAOMapper;
import fr.epitech.ktrmscls1.entities.ProfileEntity;
import fr.epitech.ktrmscls1.entities.UtilisateurEntity;
import fr.epitech.ktrmscls1.models.ProfileModel;
import fr.epitech.ktrmscls1.repositories.ProfileRepository;

@Service
public class profileDAOImpl implements ProfileDAO {

	@Autowired
	private ProfileRepository profileRepo;
	
	@Autowired
	private ProfileDAOMapper profileMapper;
	
	
	@Override
	public ProfileModel findById(Integer id) {
		Optional<ProfileEntity> profile = profileRepo.findById(id);
		if(profile.isPresent())
			return profileMapper.toModel(profile.get());
		return null;
	}

	@Override
	public List<ProfileModel> findAll(Integer id) {
		List<ProfileEntity> profile = new ArrayList<>();
		List<ProfileEntity> collect = profileRepo.findAll();
		for(int i =0; i<collect.size(); i++)
		{
			if(collect.get(i).getUtilisateur().getId()==id)
				profile.add(collect.get(i));
		}
		return profileMapper.toModel(profile);
	}

	@Override
	public ProfileModel save(ProfileModel profile) {
		// TODO Auto-generated method stub
		ProfileEntity profileAdd = profileMapper.toEntity(profile);
		profileRepo.save(profileAdd);
		return profile;
	}

	@Override
	public ProfileModel save(Integer id, ProfileModel profile) {
		if(profileRepo.existsById(id))
		{
			ProfileEntity profileAdd = profileMapper.toEntity(profile);
			profileRepo.save(profileAdd);
			return profile;
		}
		return null;
	}

	@Override
	public void Delete(Integer id) {
		try {
			profileRepo.deleteById(id);
		} catch (Exception e) {
			// TODO: handle exception
			throw new RuntimeException(e);
		}
		
	}

}
