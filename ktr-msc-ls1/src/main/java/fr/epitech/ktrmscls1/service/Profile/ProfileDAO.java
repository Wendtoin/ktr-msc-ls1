package fr.epitech.ktrmscls1.service.Profile;

import java.util.List;

import fr.epitech.ktrmscls1.entities.UtilisateurEntity;
import fr.epitech.ktrmscls1.models.ProfileModel;

public interface ProfileDAO {

	ProfileModel findById(Integer id);
	List<ProfileModel> findAll(Integer id);
	ProfileModel save(ProfileModel profile);
	ProfileModel save(Integer id,ProfileModel profile);
	void Delete(Integer id);
	
}
