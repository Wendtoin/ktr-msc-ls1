package fr.epitech.ktrmscls1.service.utilisateur;

import fr.epitech.ktrmscls1.models.UtilisateurModel;

public interface UtilisateurDAO {

	UtilisateurModel save(UtilisateurModel user);
}
