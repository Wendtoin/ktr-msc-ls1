package fr.epitech.ktrmscls1.service.utilisateur;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.epitech.ktrmscls1.MapperData.UtilisateurDAOMapper;
import fr.epitech.ktrmscls1.entities.RoleEntity;
import fr.epitech.ktrmscls1.entities.UtilisateurEntity;
import fr.epitech.ktrmscls1.models.UtilisateurModel;
import fr.epitech.ktrmscls1.repositories.RoleRepository;
import fr.epitech.ktrmscls1.repositories.UtilisateurRepository;

@Service
public class UtilisateurDAOImpl implements UtilisateurDAO{

	@Autowired
	UtilisateurRepository userRepo;
	@Autowired 
	UtilisateurDAOMapper userMapper;
	@Autowired
	RoleRepository roleRepo;
	
	@Autowired
	PasswordEncoder encode;
	
	@Override
	public UtilisateurModel save(UtilisateurModel user) {
		
		if(userRepo.findByMail(user.getMail()).isEmpty())
		{
			UtilisateurEntity usr = userMapper.toEntity(user);
			Optional<RoleEntity> role = roleRepo.findByName("UTILISATEUR");
			if(role.isPresent())
			{
				usr.getRoles().add(role.get());
				usr.setPassword(encode.encode(usr.getPassword()));
				userRepo.save(usr);
				return user;
			}
		}
		
		return null;
	}

}
