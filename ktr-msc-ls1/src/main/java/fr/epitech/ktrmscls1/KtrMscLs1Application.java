package fr.epitech.ktrmscls1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KtrMscLs1Application {

	public static void main(String[] args) {
		SpringApplication.run(KtrMscLs1Application.class, args);
	}

}
