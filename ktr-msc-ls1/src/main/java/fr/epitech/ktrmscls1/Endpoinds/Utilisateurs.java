package fr.epitech.ktrmscls1.Endpoinds;

import java.util.List;
import java.util.stream.Collectors;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.epitech.ktrmscls1.entities.RoleEntity;
import fr.epitech.ktrmscls1.models.UtilisateurModel;
import fr.epitech.ktrmscls1.repositories.RoleRepository;
import fr.epitech.ktrmscls1.security.payload.request.LoginRequest;
import fr.epitech.ktrmscls1.security.payload.response.JwtResponse;
import fr.epitech.ktrmscls1.security.securite.jwt.JwtUtils;
import fr.epitech.ktrmscls1.security.securite.userServices.UserDetailsImpl;
import fr.epitech.ktrmscls1.service.utilisateur.UtilisateurDAO;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
@RequestMapping("utilisateur")
public class Utilisateurs {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private PasswordEncoder encoder;
	
	@Autowired
	private JwtUtils jwtUtils;
	
	@Autowired
	private UtilisateurDAO userDao;
	
	@Autowired
	private RoleRepository roleRepo;
	
	@ApiOperation(value = "permet de se loger et d'avoir son token")
	@PostMapping("/")
	public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest)
	{

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		JwtResponse user = new JwtResponse(
				jwt, "Bearer", userDetails.getId(), userDetails.getUsername(), 
				roles);
		return ResponseEntity.ok(user);

	}
	
	@PostMapping("/inscription/")
	public ResponseEntity<?> inscription(@RequestBody UtilisateurModel user)
	{
		if(userDao.save(user) !=null)
			return ResponseEntity.created(null).body("User created");
		return ResponseEntity.badRequest().body("User Existed");
	}
	
	/*@GetMapping("/")
	public List<RoleEntity> role()
	{
		return roleRepo.findAll();
	}*/
}
