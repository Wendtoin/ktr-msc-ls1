package fr.epitech.ktrmscls1.dataInit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import fr.epitech.ktrmscls1.entities.RoleEntity;
import fr.epitech.ktrmscls1.models.UtilisateurModel;
import fr.epitech.ktrmscls1.repositories.RoleRepository;
import fr.epitech.ktrmscls1.service.utilisateur.UtilisateurDAO;

@Configuration
public class DataInitImpl {

	@Autowired
	private RoleRepository roleRepo;
	
	@Autowired
	private UtilisateurDAO userDao;
	
	@PostConstruct
	public void initDataInformation()
	{
		
		roleRepo.save(new RoleEntity("UTILISATEUR"));
		System.out.println("Hello bb");
		userDao.save(new UtilisateurModel("epitch@gmail.com", "epitch2021"));
	}
}
