package fr.epitech.ktrmscls1.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.epitech.ktrmscls1.entities.UtilisateurEntity;

public interface UtilisateurRepository extends JpaRepository<UtilisateurEntity, Integer>{

	Optional<UtilisateurEntity> findByMail(String email);

}
