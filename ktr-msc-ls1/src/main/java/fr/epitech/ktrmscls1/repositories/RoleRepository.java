package fr.epitech.ktrmscls1.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.epitech.ktrmscls1.entities.RoleEntity;

public interface RoleRepository extends JpaRepository<RoleEntity, Integer>{

	Optional<RoleEntity> findByName(String name);
	
}
