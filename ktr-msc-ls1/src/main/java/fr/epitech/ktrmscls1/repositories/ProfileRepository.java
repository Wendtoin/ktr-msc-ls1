package fr.epitech.ktrmscls1.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.epitech.ktrmscls1.entities.ProfileEntity;

public interface ProfileRepository extends JpaRepository<ProfileEntity, Integer>{

	@Query("SELECT p FROM ProfileEntity p WHERE p.utilisateur.id =:id")
	List<ProfileEntity> findByIdCollect(@Param("id") Integer id);
	
}
