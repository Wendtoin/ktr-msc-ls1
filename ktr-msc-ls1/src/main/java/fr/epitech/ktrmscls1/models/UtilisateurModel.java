package fr.epitech.ktrmscls1.models;

public class UtilisateurModel {
	private Integer id;
	private String mail;
	private String password;
	public UtilisateurModel(Integer id, String mail, String password) {
		super();
		this.id = id;
		this.mail = mail;
		this.password = password;
	}
	public UtilisateurModel(String mail, String password) {
		super();
		this.mail = mail;
		this.password = password;
	}
	public UtilisateurModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
