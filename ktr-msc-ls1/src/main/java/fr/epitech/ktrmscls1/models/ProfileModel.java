package fr.epitech.ktrmscls1.models;

public class ProfileModel {

	private Integer id;
	private String name;
	private String nameCompany;
	private String email;
	private String telephone;
	private UtilisateurModel utilisateur;
	public ProfileModel(Integer id, String name, String nameCompany, String email, String telephone, UtilisateurModel utilisateur) {
		super();
		this.id = id;
		this.name = name;
		this.nameCompany = nameCompany;
		this.email = email;
		this.telephone = telephone;
		this.utilisateur = utilisateur;
	}
	public ProfileModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ProfileModel(String name, String nameCompany, String email, String telephone, UtilisateurModel utilisateur) {
		super();
		this.name = name;
		this.nameCompany = nameCompany;
		this.email = email;
		this.telephone = telephone;
		this.utilisateur = utilisateur;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNameCompany() {
		return nameCompany;
	}
	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public UtilisateurModel getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(UtilisateurModel utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	
}
