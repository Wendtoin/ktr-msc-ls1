package fr.epitech.ktrmscls1.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "profile")
public class ProfileEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private String nameCompany;
	private String email;
	private String telephone;
	@ManyToOne
	private UtilisateurEntity utilisateur;
	public ProfileEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ProfileEntity(Integer id, String name, String nameCompany, String email, String telephone,
			UtilisateurEntity utilisateur) {
		super();
		this.id = id;
		this.name = name;
		this.nameCompany = nameCompany;
		this.email = email;
		this.telephone = telephone;
		this.utilisateur = utilisateur;
	}
	public ProfileEntity(String name, String nameCompany, String email, String telephone,
			UtilisateurEntity utilisateur) {
		super();
		this.name = name;
		this.nameCompany = nameCompany;
		this.email = email;
		this.telephone = telephone;
		this.utilisateur = utilisateur;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNameCompany() {
		return nameCompany;
	}
	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public UtilisateurEntity getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(UtilisateurEntity utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	
	
	
}
